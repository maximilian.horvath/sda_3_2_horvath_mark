// Horvath_Mark.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <stdio.h>
#include <stdlib.h>

typedef struct CHESTII {
	char nume[10];
	int a, b;

}CHESTII;
int main()
{
	FILE* f = NULL;
	f = fopen("file.in.txt", "r");
	if (f == NULL)
	{
		perror(NULL);
		exit(-1);
	}
	int n;
	fscanf(f, "%d", &n);
	CHESTII v[n];

	for (int i = 0; i < n; i++)
	{
		fscanf(f, "%s %d %d", v[i].nume, v[i].a, v[i].b);
		printf("%s %d\n", v[i].nume, v[i].a + v[i].b);
	}
	return 0;
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
