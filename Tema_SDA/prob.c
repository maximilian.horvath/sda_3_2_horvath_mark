#include <stdio.h>
#include <stdlib.h>

//CURS1-EX3
int putere(int baza, int exponent)
{
	int rez = 1;
	if (exponent == 0)
		return 1;
	else if (baza == 0)
		return 0;
	for (int i = 0; i < exponent; i++)
	{
		rez = rez * baza;
	}
	return rez;

}
void bisect(int an)
{
	if ((an % 4 == 0 && an % 100 != 0) || (an % 400 == 0))
		printf("An bisect.\n");
	else
		printf("NU e bisect.\n");

}

//CURS 3-EX4

int factorial(int n)
{
	int k = 1;
	if (n == 0)
		return k;
	for (int i = 1; i <= n; i++)
	{
		k = k * i;
	}
	return k;
}
//CURS 4-EX2
void function(int n)
{
	while (n >= 1) {
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= n; j++)
				printf("*");
		}
		n = n - 3;

	}

}

//CURS 5-EX2
void insertion_sort_reverse(tip_element a[], int n)
{
	int i, j;
	tip_element tmp;
	for (i = n - 2; i >= 0; i--)  // �ncepem de la penultimul element ?i mergem spre �nceput
	{
		tmp = a[i];              // Salv�m elementul curent
		for (j = i; (j < n - 1) && (tmp.cheie > a[j + 1].cheie); j++)
		{
			a[j] = a[j + 1];     // Mut�m elementele spre st�nga dac� sunt mai mari dec�t tmp
		}
		a[j] = tmp;              // Inser�m elementul curent �n pozi?ia corect�
	}
}

//CURS 8-EX1 
typedef struct LIST {
	int elem;
	struct LIST* next;
}LIST;

LIST* stergere(LIST* lista, int elem)
{
	LIST* p = NULL;
	if (lista == NULL)
		return NULL;
	if (lista->next == NULL)
	{
		free(lista);
		return NULL;
	}
	if (lista->elem == elem)
	{
		p = lista;
		lista = lista->next;
		free(p);
		return lista;

	}
	p = lista->next;
	LIST* prev = lista;
	while (p != NULL)
	{
		if (p->elem == elem)
		{
			prev->next = p->next;
			free(p);
			break;
		}
		p = p->next;
		prev = prev->next;

	}
	return lista;


}
//CURS 8-EX2 DUBLU INLANTUIT
typedef struct LIST {
	int elem;
	struct LIST* next, * prev;
}LIST;
LIST* stergere_inceput(LIST* lista)
{
	if (lista == NULL)
		return NULL;
	LIST* aux = lista;
	lista = lista->next;
	if (lista != NULL)
		lista->prev = NULL;
	free(aux);
	return lista;
}
LIST* stergere_random(LIST* lista, int elem)
{
	LIST* p = lista;
	if (lista == NULL)
		return NULL;
	if (lista->elem== elem)
		return stergere_inceput(lista);
	while (p != NULL)
	{
		if (p->elem== elem)
		{
			if (p->next != NULL)
			{
				p->prev->next = p->next;
				p->next->prev = p->prev;
			}
			else
			{
				p->prev->next = NULL;
				free(p);
				return lista;
			}
		}
		p = p->next;
	}
	return lista;


}

//CURS 8- EX4

LIST* inversare(LIST* lista)
{
	LIST* prev = NULL, * q = NULL, * p = NULL;
	prev = lista;
	p = lista->next;
	prev->next = NULL;  // prima sagetuta arata spre NULL acum
	while (p != NULL)
	{
		q = p;
		p = p->next;
		q->next = prev;;
		prev = q;
	}
	lista = prev;/// ca lista sa pointeze spre ultimul (care e acum primul nod)
	return lista;

}

//CURS 9-EX1
int count_nod(LIST* p)
{
	if (p == NULL)
		return 0;
	return 1 + count_nod(p->next);
}


int main()
{
	int baza, exponent,an;
	/*scanf("%d", &baza);
	scanf("%d", &exponent);
	scanf("%d", &an);

	printf("%d", putere(baza, exponent));
	bisect(an);*/
	int n;
	scanf("%d", &n);
	//printf("%d", factorial(n));
	function(n);
	return 0;
}