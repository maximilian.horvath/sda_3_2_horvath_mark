#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct Student {
	char nume_st[20];
	int id;
	char gen;
	struct Student* next_student;
}Student;
typedef struct Uni {
	char nume[30];
	char adresa[100];
	int nr_studenti;
	struct Student* lista_student;
	struct Uni* next_uni;
}Uni;

Student* creaza_student(char nume_st, int id, char gen)
{
	Student* aux = NULL;
	aux = (Student*)malloc(sizeof(Student));
	if (aux == NULL)
	{
		perror(NULL);
		exit(-1);
	}
	strcpy(aux->nume_st, nume_st);
	aux->id = id;
	aux->gen = gen;
	aux->next_student =NULL;
	return aux;
}	
Uni* creaza_uni(char nume, char adresa, int nr_studenti)
{
	Uni* nou_uni = NULL;
	nou_uni = (Uni*)malloc(sizeof(Uni));
	if (nou_uni == NULL)
	{
		perror(NULL);
		exit(-1);
	}
	strcpy(nou_uni->nume, nume);
	strcpy(nou_uni->adresa, adresa);
	nou_uni->nr_studenti = nr_studenti;
	nou_uni->next_uni = NULL;
	return nou_uni;
}

void add_uni(Uni** lista, Uni* o_universitate, char nume, char adresa, int nr_studenti)
{
	if (*lista == NULL)
	{
		*lista = o_universitate;
	}
	else
	{
		creaza_uni(nume, adresa, nr_studenti);
		Uni* p = *lista;
		while (p->next_uni != NULL)
		{
			if (strcmp(p->nume, o_universitate->nume) == 0)
				break;
			p = p->next_uni;
		}
		p->next_uni = o_universitate;
	}
	//return lista;
}

void add_student( Uni** lista, char *nume_uni,char *nume_student,int id,char gen)
{
	int ok = 0,k=0;
	if (*lista == NULL)
	{
		printf("NU EXISTA UNIVERSITATI");
		return;
	}
	else
	{
		
		Uni* p = *lista;
		Student* q = p;
		while (p->next_uni != NULL)
		{ 
			ok = 0;
			while (q->next_student != NULL)
			{
				if (q->id == id)
				{
					ok = 1; 
					break;
				}
				q = q->next_student;
			}
			if (ok == 1)
			{
				k = 1;
				break;
			}
			p = p->next_uni;

		}
		if (k == 1)
			return;
		else 
		{
			p = *lista;
			while (p->next_uni!=NULL)
			{
				if (strcmp(p->nume, nume_uni) == 0)
					break;
				p = p->next_uni;
			}
			q = p;
			q->next_student = creaza_student(nume_student, id, gen);

		}
		
	}
	//return lista;
}
void afis(Uni* lista)
{

	Uni* p = lista;
	while (p)
	{
		printf("Univers: %s, Adresa :%s\n", p->nume, p->adresa);
		Student* q = p->lista_student;
		while (q)
		{
			printf("Studern :%s, id: %d, gen: %c/n", q->nume_st, q->id, q->gen);
			q = q->next_student;
		}
		p = p->next_uni;
	}
}
void stergeStudent(Student* lista)
{
	while (lista) {
		Student* temp;
		lista = lista->next_student;
		free(temp);
	}

}
void stergeUni(Uni* lista)
{
	while (lista)
	{
		Uni* temp = lista;
		lista = lista->next_uni;
		stergeStudent(temp->lista_student);
		free(temp);
	}
}
int main()
{
	Uni* lista = NULL;
	Uni *u1 = creaza_uni("UPT", "str Odobescu", 300);
	Uni* u2 = creaza_uni("Meicina", "str ceva", 500);

	add_uni(lista, u1,"Alex", 48, "W");
	add_uni(lista, u2,"Mario", 45, "M");

	Student* s1 = creaza_student("Alexia", 89, "W");
	Student* s2 = creaza_student("levi", 94, "M");
	Student* s3 = creaza_student("Tobias", 63, "M");

	add_student(&lista, "UPT", "Mario",45,"M");

	return 0;

}

