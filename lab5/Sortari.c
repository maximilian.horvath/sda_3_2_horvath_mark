#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct MYFILE {
	char isDir;//un fel de bool ce are val 0 sau 1 
	char nume[256];
	unsigned int size;

}MYFILE;

// isdir:0
//nume:/ceva/asd.txt"
//size:4099
//Selection sort

//sa fie sortate directoarele inaintea fisierelor 
//daca sunt de acelasi tip (<isDir>) credscator dupa size
//alfabetic (** daca au acelasi size)


void swap(MYFILE* el1, MYFILE* el2)
{
	MYFILE aux;
	aux = *el1;
	*el1 = *el2;
	*el2 = aux;
}

int comparare(MYFILE a, MYFILE b)
{
	if (a.isDir > b.isDir)
		return 1;
	else if (a.isDir == b.isDir)
	{
		if(a.size<b.size)
			return 1;
		else if (a.size == b.size)
		{
			if (strcmp(a.nume, b.nume) < 0)
				return 1;
		}
	}
	return 0;
}
void selection_sort(MYFILE *a, int n)
{
	int i, j, min; // min retine INDEXUL elementului cu valoare minima
	for (i = 0; i < n - 1; i++)
	{
		min = i;
		for (j = i + 1; j < n; j++)
		{
			if (comparare(a[j],a[min]))
				min = j;
		}
		swap(&a[min], &a[i]);
	}
}

void quicksort(MYFILE *a, int prim, int ultim)
{
	int stanga = prim + 1;
	int dreapta = ultim;
	//alegere pivot
	//mutare pivot pe prima pozitie
	MYFILE pivot = a[prim];
	while (stanga <= dreapta) //partitionare
	{
		while (comparare(a[stanga],pivot))
			stanga++;
		while (comparare(pivot, a[dreapta]))
			dreapta--;
		if (stanga < dreapta)
			swap(&a[stanga++], &a[dreapta--]);
		else
			stanga++;
	}
	//mutare pivot la locul sau final
	swap(&a[dreapta], &a[prim]);
	//apelurile recursive
	if (prim < dreapta - 1)
		quicksort(a, prim, dreapta - 1);
	if (dreapta + 1 < ultim)
		quicksort(a, dreapta + 1, ultim);
}

void insertion_sort(MYFILE *a,int prim,int ultim)
{
	int i, j;
	MYFILE tmp;
	for (i = prim; i < ultim+1; i++)
	{
		tmp = a[i];
		for (j = i; (j > 0) && (comparare(tmp,a[j-1])); j--)
			a[j] = a[j - 1];
		a[j] = tmp;
	}
}
void quicksort_modify(MYFILE* a, int prim, int ultim)
{
	int stanga = prim + 1;
	int dreapta = ultim;
	//alegere pivot
	//mutare pivot pe prima pozitie
	if (dreapta - stanga < 7)
	{
		insertion_sort(a, stanga, dreapta);
	}
	else
	{
		MYFILE pivot = a[prim];
		while (stanga <= dreapta) //partitionare
		{
			while (comparare(a[stanga], pivot))
				stanga++;
			while (comparare(pivot, a[dreapta]))
				dreapta--;
			if (stanga < dreapta)
				swap(&a[stanga++], &a[dreapta--]);
			else
				stanga++;
		}
		//mutare pivot la locul sau final
		swap(&a[dreapta], &a[prim]);
		//apelurile recursive
		if (prim < dreapta - 1)
			quicksort(a, prim, dreapta - 1);
		if (dreapta + 1 < ultim)
			quicksort(a, dreapta + 1, ultim);
	}
}
void tipar(MYFILE* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("dir :%d		nume: %s	size: %d\n",a[i].isDir,a[i].nume,a[i].size);
	}
	printf("--------------------------------------\n");
}
int main()
{

	MYFILE f[10] = {{ 0,"acadea",512 }, { 1,"mark",256 },{ 1,"max",1024 },{ 0,"abc",352 },{ 0,"abcd",352 },{ 1,"abrac",231 },{ 1,"na uhhh",1024 } };
	tipar(f, 7);
	selection_sort(f, 7);
	tipar(f, 7);
	quicksort(f, 0,6);
	tipar(f, 7);
	quicksort_modify(f, 0, 6);
	tipar(f, 7);
	return 0;
}