#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct FILE {
	char isDir;//un fel de bool ce are val 0 sau 1 
	char nume[256];
	unsigned int size;

}FILE;

// isdir:0
//nume:/ceva/asd.txt"
//size:4099
//Selection sort

//sa fie sortate directoarele inaintea fisierelor 
//daca sunt de acelasi tip (<isDir>) credscator dupa size
//alfabetic (** daca au acelasi size)


void swap(FILE* el1, FILE* el2)
{
	FILE aux;
	aux = *el1;
	*el1 = *el2;
	*el2 = aux;
}

int comparare(FILE a, FILE b)
{
	if (a.isDir > b.isDir)
		return 1;
	else if (a.isDir == b.isDir)
	{
		if (a.size < b.size)
			return 1;
		else if (a.size == b.size)
		{
			if (strcmp(a.nume, b.nume) < 0)
				return 1;
		}
	}
	return 0;
}
void selection_sort(FILE* a, int n)
{
	int i, j, min; // min retine INDEXUL elementului cu valoare minima
	for (i = 0; i < n - 1; i++)
	{
		min = i;
		for (j = i + 1; j < n; j++)
		{
			if (compar(a[j], a[min]))
				min = j;
		}
		swap(&a[min], &a[i]);
	}
}
void tipar(FILE* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("dir :%d		nume: %s	size: %d\n", a[i].isDir, a[i].nume, a[i].size);
	}
}
int main()
{

	FILE f[10] = { { "0","/ceva/ast.txt",256 }, { "1","/home/mark",512 },{ "1","/main/max",1024 } };
	selection_sort(f, 3);
	tipar(f, 3);
	return 0;
}